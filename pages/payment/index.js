import styles from "./Payment.module.scss";
import Link from "next/link";
import Head from "next/head";
import Image from "next/image";

const Payment = () => {
  return (
    <>
      <Head>
        <title>Photojet | Оплата</title>
        <meta name="title" content="Pjotojet"></meta>
      </Head>
      <div className={styles.container}>
        <section className={styles.content}>
          <nav className={styles.navBar}>
            <Link href="/faq" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Часто задаваемые вопросы</a>
              </span>
            </Link>
            <Link href="/how-the-service-works" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Как работает сервис</a>
              </span>
            </Link>
            <Link href="/service-cancellation" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Отказ от услуги</a>
              </span>
            </Link>
            <Link href="/payment" legacyBehavior>
              <span className={styles.navTitleActive}>
                <a>Оплата</a>
              </span>
            </Link>
            <Link href="/contacts" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Контакты</a>
              </span>
            </Link>
            <Link href="/feedback" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Обратная связь</a>
              </span>
            </Link>
            <Link href="/recruiting" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Работайте с нами</a>
              </span>
            </Link>
          </nav>

          <article className={styles.mainContent}>
            <h1 className={styles.title}>Оплата</h1>
            <div className={styles.textWrapper}>
              <span className={styles.text}>
                Для оплаты услуг наш менеджер пришлёт Вам ссылку на платёжную
                страницу.
              </span>
              <span className={styles.text}>
                К оплате принимаются платежные карты:
                <strong>VISA Inc, MasterCard WorldWide, МИР.</strong>
              </span>
              <ol>
                <div className={styles.textTitle}>
                  При оплате заказа банковской картой, обработка платежа
                  происходит на авторизационной странице банка, где Вам
                  необходимо ввести данные Вашей банковской карты:
                </div>

                <li className={styles.textWithMargin}>тип карты</li>
                <li className={styles.textWithMargin}>номер карты</li>
                <li className={styles.textWithMargin}>
                  срок действия карты (указан на лицевой стороне карты)
                </li>
                <li className={styles.textWithMargin}>
                  Имя держателя карты (латинскими буквами, точно также как
                  указано на карте)
                </li>
                <li className={styles.textWithMargin}>CVC2/CVV2 код</li>
              </ol>
              <Image
                className={styles.cardImg}
                src="/bankCard.png"
                width={832}
                height={492}
                alt="image of bank card"
              ></Image>
              <div className={styles.textWrapper}>
                <span className={styles.text}>
                  Если Ваша карта подключена к услуге 3D-Secure, Вы будете
                  автоматически переадресованы на страницу банка, выпустившего
                  карту, для прохождения процедуры аутентификации. Информацию о
                  правилах и методах дополнительной идентификации уточняйте в
                  Банке, выдавшем Вам банковскую карту. Безопасность обработки
                  интернет-платежей через платежный шлюз банка гарантирована
                  международным сертификатом безопасности PCI DSS. Передача
                  информации происходит с применением технологии шифрования SSL.
                  Эта информация недоступна посторонним лицам.
                </span>
                <span className={styles.text}>
                  <strong>
                    Советы и рекомендации по необходимым мерам безопасности
                    проведения платежей с использованием банковской карты:
                  </strong>
                </span>
                <ol className={styles.textWrapper}>
                  <li className={styles.textWithMargin}>
                    Берегите свои пластиковые карты так же, как бережете
                    наличные деньги. Не забывайте их в машине, ресторане,
                    магазине и т.д.
                  </li>
                  <li className={styles.textWithMargin}>
                    Никогда не передавайте полный номер своей кредитной карты по
                    телефону каким-либо лицам или компаниям.
                  </li>
                  <li className={styles.textWithMargin}>
                    Всегда имейте под рукой номер телефона для экстренной связи
                    с банком, выпустившим вашу карту, и в случае ее утраты
                    немедленно свяжитесь с банком.
                  </li>
                  <li className={styles.textWithMargin}>
                    Вводите реквизиты карты только при совершении покупки.
                    Никогда не указывайте их по каким-то другим причинам.
                  </li>
                </ol>
              </div>
              <article className={styles.paymentsLogo}>
                <Image
                  src="/paymentsLogo/mastercard.png"
                  width={40}
                  height={24}
                  alt="mastercard icon"
                />
                <Image
                  src="/paymentsLogo/visa.png"
                  width={62}
                  height={21}
                  alt="visa icon"
                />
                <Image
                  src="/paymentsLogo/mir.png"
                  width={73}
                  height={20}
                  alt="mir icon"
                />
              </article>
            </div>
          </article>
        </section>
      </div>
    </>
  );
};
export default Payment;
