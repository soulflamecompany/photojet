import styles from "./Service.module.scss";
import Link from "next/link";
import Head from "next/head";

const Service = () => {
  return (
    <>
      <Head>
        <title>Photojet | Как работает сервис</title>
        <meta name="title" content="Pjotojet"></meta>
      </Head>
      <div className={styles.container}>
        <section className={styles.content}>
          <nav className={styles.navBar}>
            <Link href="/faq" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Часто задаваемые вопросы</a>
              </span>
            </Link>
            <Link href="/how-the-service-works" legacyBehavior>
              <span className={styles.navTitleActive}>
                <a>Как работает сервис</a>
              </span>
            </Link>
            <Link href="/service-cancellation" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Отказ от услуги</a>
              </span>
            </Link>
            <Link href="/payment" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Оплата</a>
              </span>
            </Link>
            <Link href="/contacts" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Контакты</a>
              </span>
            </Link>
            <Link href="/feedback" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Обратная связь</a>
              </span>
            </Link>
            <Link href="/recruiting" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Работайте с нами</a>
              </span>
            </Link>
          </nav>

          <article className={styles.mainContent}>
            <h1 className={styles.title}>
              Получить фотосессию мечты – просто!
            </h1>
            <ul className={styles.textWrapper}>
              <li className={styles.text}>
                Выберете тематику фотосессии на главной странице
              </li>
              <li className={styles.text}>
                Заполните короткую форму. Укажите свое имя, email, телефон.
              </li>
              <li className={styles.text}>
                Отдельное внимание уделите выбору даты фотосессии и локации, где
                она будет проводиться. Также с помощью формы вы можете изменить
                тематику фотосессии. 
              </li>
              <li className={styles.text}>
                После проверки введенных данных нажмите на кнопку «Отправить
                заявку» и ожидайте звонка менеджера Photojet в течение
                нескольких минут.
              </li>
            </ul>
          </article>
        </section>
      </div>
    </>
  );
};

export default Service;
