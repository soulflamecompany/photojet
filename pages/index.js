import Head from "next/head";
import styles from "../styles/Home.module.css";
import Hero from "../components/Hero/Hero";
import PhotoShoot from "../components/PhotoShoot/PhotoShoot";
import FormOrder from "../components/FormOrder/FormOrder";
import React, { useState } from "react";

export default function Home() {
  const [formOpened, setFormOpened] = useState(false);

  return (
    <>
      <Head>
        <title>Photojet | Главная</title>
        <meta name="title" content="Pjotojet"></meta>
      </Head>
      <div className={styles.container}>
        <Hero />
        <PhotoShoot onClickForm={() => setFormOpened(true)} />
        {formOpened ? <FormOrder onClose={() => setFormOpened(false)} /> : null}
      </div>
    </>
  );
}
