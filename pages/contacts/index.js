import styles from "./Contacts.module.scss";
import Link from "next/link";
import Head from "next/head";

const Contacts = () => {
  return (
    <>
      <Head>
        <title>Photojet | Контакты</title>
        <meta name="title" content="Pjotojet"></meta>
      </Head>
      <div className={styles.container}>
        <section className={styles.content}>
          <nav className={styles.navBar}>
            <Link href="/faq" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Часто задаваемые вопросы</a>
              </span>
            </Link>
            <Link href="/how-the-service-works" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Как работает сервис</a>
              </span>
            </Link>
            <Link href="/service-cancellation" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Отказ от услуги</a>
              </span>
            </Link>
            <Link href="/payment" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Оплата</a>
              </span>
            </Link>
            <Link href="/contacts" legacyBehavior>
              <span className={styles.navTitleActive}>
                <a>Контакты</a>
              </span>
            </Link>
            <Link href="/feedback" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Обратная связь</a>
              </span>
            </Link>
            <Link href="/recruiting" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Работайте с нами</a>
              </span>
            </Link>
          </nav>

          <article className={styles.mainContent}>
            <h1 className={styles.title}>Контакты</h1>
            <nav className={styles.contactsWrapper}>
              <ul>
                <li className={styles.text}>ООО « МАРТ »</li>
                <li className={styles.text}>ИНН: 631416464654</li>
                <li className={styles.text}>КПП: 633654654654</li>
                <li className={styles.text}>ОГРН: 126546547</li>
                <li className={styles.text}>
                  443099, Россия, г. Москва, ул. Арбат, д. 57, ком. 2
                </li>
                <li className={styles.text}>
                  <a href="mailto:feedback@photojet.com">
                    feedback@photojet.com
                  </a>
                </li>
                <li className={styles.text}>
                  <a href="tel:+79999999999"> Телефон: +79999999999</a>
                </li>
              </ul>
            </nav>

            <div>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1414.6718409316604!2d37.58320001693979!3d55.747182228374996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54bb1574d30ab%3A0x2baeb4158ef3e153!2z0YPQuy4g0JDRgNCx0LDRgiwgNTcsINCc0L7RgdC60LLQsCwgMTE5MDAy!5e0!3m2!1sru!2sru!4v1678986462290!5m2!1sru!2sru"
                width="800"
                height="320"
                className={styles.map}
                loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"
              ></iframe>
            </div>
          </article>
        </section>
      </div>
    </>
  );
};
export default Contacts;
