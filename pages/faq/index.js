import styles from "./FAQ.module.scss";
import Link from "next/link";
import Head from "next/head";
import Image from "next/image";
import { useState } from "react";

const FAQ = () => {
  const [show, setShow] = useState(0);

  const toggle = (index) => {
    if (show === index) {
      return setShow(null);
    }

    setShow(index);
  };

  const faq = [
    {
      question: "Могу ли я выбрать свою локацию?",
      answer:
        "Конечно! При оформлении заказа оператор Photojet учтет все пожелания, включая локацию, выбор фотографа и образа. Мы помогаем в организации фотосессий любой сложности.",
    },
    {
      question: "Что надеть на фотосессию?",
      answer:
        "Мы рекомендуем выбирать одежду, в которой вы чувствуете себя максимально комфортно. При необходимости, вы можете заказать дополнительные услуги по подбору образа от стилиста и/или услуги визажиста.",
    },
    {
      question: "Сколько времени занимает фотосессия?",
      answer:
        "Это зависит от типа, сценария и сложности фотосессии. В среднем фотосессия длится от 1 до 2 часов. Специалисты Photojet готовы уделить вам любое количество времени, потому что главное для нас – результат, превосходящий ваши ожидания.",
    },
    {
      question: "Как получить фотографии после съемки?",
      answer:
        "Еще до начала фотосъемки мастер-фотограф обсуждает с клиентом детали заказа: будет ли производится ретушь фотографий, как клиенту удобнее получить результат, нужна ли печать фото. Вы можете выбрать любой удобный способ получения фотографий. Чаще всего, фотографы отправляют фотографии с помощью физических носителей или файлообменников (например, Google Drive). ",
    },
  ];

  return (
    <>
      <Head>
        <title>Photojet | Часто задаваемые вопросы</title>
        <meta name="title" content="Pjotojet"></meta>
      </Head>
      <div className={styles.container}>
        <section className={styles.content}>
          <nav className={styles.navBar}>
            <Link href="/faq" legacyBehavior>
              <span className={styles.navTitleActive}>
                <a>Часто задаваемые вопросы</a>
              </span>
            </Link>
            <Link href="/how-the-service-works" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Как работает сервис</a>
              </span>
            </Link>
            <Link href="/service-cancellation" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Отказ от услуги</a>
              </span>
            </Link>
            <Link href="/payment" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Оплата</a>
              </span>
            </Link>
            <Link href="/contacts" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Контакты</a>
              </span>
            </Link>
            <Link href="/feedback" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Обратная связь</a>
              </span>
            </Link>
            <Link href="/recruiting" legacyBehavior>
              <span className={styles.navTitle}>
                <a>Работайте с нами</a>
              </span>
            </Link>
          </nav>

          <article className={styles.mainContent}>
            <h1 className={styles.title}>Часто задаваемые вопросы</h1>

            {faq.map((item, index) => (
              <article
                className={styles.questionCard}
                onClick={() => toggle(index)}
                key={index}
              >
                <div className={styles.questionWrapper}>
                  <div className={styles.question}>{item.question}</div>
                  <div>
                    {show === index ? (
                      <Image
                        className={styles.closeIcon}
                        src="closeIconFaq.svg"
                        alt="icon close"
                        width={24}
                        height={24}
                      />
                    ) : (
                      <Image
                        className={styles.openIcon}
                        src="openIconFaq.svg"
                        alt="icon open"
                        width={24}
                        height={24}
                      />
                    )}
                  </div>
                </div>
                {show === index && (
                  <div className={styles.answer}>{item.answer}</div>
                )}
              </article>
            ))}
          </article>
        </section>
      </div>
    </>
  );
};

export default FAQ;
