import Link from "next/link";
import styles from "./Header.module.scss";

const Header = () => {
  return (
    <header className={styles.container}>
      <section className={styles.content}>
        <Link href="/" legacyBehavior>
          <a className={styles.logo}>Photojet</a>
        </Link>
        <nav>
          <ul className={styles.navigation_list}>
            <li>
              <Link href="/personal" legacyBehavior>
                <a>Персональная</a>
              </Link>
            </li>
            <li>
              <Link href="/" legacyBehavior>
                <a>Бизнес</a>
              </Link>
            </li>
            <li>
              <Link href="/" legacyBehavior>
                <a>«История любви»</a>
              </Link>
            </li>
            <li>
              <Link href="/" legacyBehavior>
                <a>Семейная</a>
              </Link>
            </li>
            <li>
              <Link href="/" legacyBehavior>
                <a>Модельная</a>
              </Link>
            </li>
          </ul>
        </nav>
      </section>
    </header>
  );
};
export default Header;
